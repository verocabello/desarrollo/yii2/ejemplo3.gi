﻿CREATE DATABASE IF NOT EXISTS jota;
USE jota;

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  foto varchar(100),
  descripcion varchar(250),
  precio float,
  oferta boolean,
  PRIMARY KEY(id)
);

INSERT INTO productos VALUES 
  (1,'taza1','taza1.png','Taza Original',10,TRUE),
  (2,'taza2','taza2.png','Taza Patos',30,FALSE),
  (3,'taza3','taza3.png','Taza Gato Borde',50,FALSE),
  (4,'taza4','taza4.png','Taza Unicornio',40,FALSE),
  (5,'taza5','taza5.png','Taza Original',20,TRUE);


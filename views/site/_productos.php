<?php
$this->title = 'Productos';
?>
<div class="container">
<div class="row">
    
    <div class="col-sm-4">

        <?php
        use yii\helpers\Html;
        
        echo Html::img("@web/imgs/".$foto,[
            "class" => "fotoproductos"
        ]);
        ?>
        <div class="container">
            <p><b><?= $id ?> - <?= $nombre ?></b></p>
        
            <p><?= $descripcion ?></p>
        
            <p><?= $precio ?></p>
        </div>
    </div>
</div>
</div>


<?php

//use yii\grid\GridView;
//
//echo GridView::widget([
//    'dataProvider' => $datos,
//    'columns' => [
//        'id',
//        'nombre',
//        'foto',
//        'descripcion',
//        'precio'
//    ]
//]);


$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    
<?php
//    use yii\helpers\Html;
//    
//    echo Html::img("@web/imgs/taza1.png",[
//        "class" => "fotoTaza1"
//    ]);

    
    foreach ($productos as $k=>$producto){
        echo $this -> render ("_productos",[
            "id" => $producto -> id,
            "nombre" => $producto -> nombre,
            "foto" => $producto -> foto,
            "descripcion" => $producto -> descripcion,
            "precio" => $producto -> precio,
        ]);
    }
?>

</div>
